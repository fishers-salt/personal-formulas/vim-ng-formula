# frozen_string_literal: true

control 'vim-ng-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'vim-ng-config-file-zsh-env-include-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/env_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'vim-ng-config-file-editor-zsh-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/env_includes/editor.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('if command -v vim > /dev/null; then') }
    its('content') { should include('export GIT_EDITOR=${PREFERRED_EDITOR}') }
  end
end
