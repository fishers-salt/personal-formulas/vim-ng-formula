# frozen_string_literal: true

control 'vim-ng-package-install-pkg-installed' do
  title 'it should be installed'

  describe package('vim') do
    it { should be_installed }
  end
end
