# frozen_string_literal: true

control 'vim-ng-config-clean-editor-zsh-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/env_includes/editor.zsh') do
    it { should_not exist }
  end
end
