# frozen_string_literal: true

control 'vim-ng-package-clean-pkg-absent' do
  title 'should not be installed'
  describe package('vim') do
    it { should_not be_installed }
  end
end
