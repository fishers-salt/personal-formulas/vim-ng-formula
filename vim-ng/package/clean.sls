# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as vim_ng with context %}

include:
  - {{ sls_config_clean }}

vim-ng-pacakge-clean-pkg-absent:
  pkg.removed:
    - name: {{ vim_ng.pkg.name }}
