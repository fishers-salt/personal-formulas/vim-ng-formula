# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as vim_ng with context %}

vim-ng-package-install-pkg-installed:
  pkg.installed:
    - name: {{ vim_ng.pkg.name }}
