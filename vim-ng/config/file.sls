# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as vim_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('vim-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_vim-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

vim-ng-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

{%- set preferred_editor = user.get('preferred_editor', None) -%}
{% if preferred_editor == "vim" %}
vim-ng-config-file-zsh-env-include-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/env_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - vim-ng-config-file-user-{{ name }}-present

vim-ng-config-file-editor-zsh-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/env_includes/editor.zsh
    - source: {{ files_switch([
                   'zsh/' ~ name ~ 'editor.zsh.tmpl',
                   'zsh/editor.zsh.tmpl'],
                 lookup='vim-ng-config-file-editor-zsh-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - vim-ng-config-file-zsh-env-include-{{ name }}-managed
{% endif %}
{% endif %}
{% endfor %}
{% endif %}
